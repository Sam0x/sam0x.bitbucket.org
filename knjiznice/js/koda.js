
var baseUrl = 'https://rest.ehrscape.com/rest/v1';
var queryUrl = baseUrl + '/query';

var username = "ois.seminar";
var password = "ois4fri";

var musketirji = [
  ["Janez", "Novak", "1960-10-11", "men","a"],
  ["Marija", "Stare","1956-01-01","women","a"],
  ["Zdravko", "Svetovljan", "1971-05-23","men","a"]
];

var rezultati = [ ["2014-12-03T09:16Z", "180","81","36.3","120","90","40",""], ["2015-01-04T09:18Z", "180","83","36.5","122","91","43",""],["2015-01-31T10:10Z", "181","85","36.7","131","91","41",""],
["2011-03-03T11:06Z", "170","90","36.5","140","95","39",""],["2011-04-11T08:16Z", "171","92","37.0","134","93","43",""], ["2011-05-12T09:13Z", "171","95","38.0","136","91","44",""],
["2016-05-05T06:16Z", "185","70","36.4","110","90","80",""],["2016-06-12T09:26Z", "186","69.5","36.4","115","89","70",""],["2016-07-13T10:16Z", "185","71","36.7","115","85","79",""]
];

var pogled=0;

/**
 * Prijava v sistem z privzetim uporabnikom za predmet OIS in pridobitev
 * enolične ID številke za dostop do funkcionalnosti
 * @return enolični identifikator seje za dostop do funkcionalnosti
 */
function getSessionId() {
    var response = $.ajax({
        type: "POST",
        url: baseUrl + "/session?username=" + encodeURIComponent(username) +
                "&password=" + encodeURIComponent(password),
        async: false
    });
    return response.responseJSON.sessionId;
}



/**
 * Kreiraj nov EHR zapis za pacienta in dodaj osnovne demografske podatke.
 * V primeru uspešne akcije izpiši sporočilo s pridobljenim EHR ID, sicer
 * izpiši napako.
 */
function kreirajEHRzaBolnika(st) {

	if(st==0){
		sessionId = getSessionId();
		var ime = $("#kreirajIme").val();
		var priimek = $("#kreirajPriimek").val();
		var datumRojstva = $("#kreirajDatumRojstva").val();
		var spol=$("#kreirajSpol").val();
		if(spol.length!=0){
			if(spol.localeCompare("Moški")==0){
				spol="men";
			}
			else{
				spol="women";
			}
		}
	
	
		if (!ime || !priimek || !datumRojstva || ime.trim().length == 0 ||
	      priimek.trim().length == 0 || datumRojstva.trim().length == 0 || spol.length==0) {
			$("#kreirajSporocilo").html("<span class='obvestilo label " +
	      "label-warning fade-in'>Prosim vnesite zahtevane podatke!</span>");
		} else {
			$.ajaxSetup({
			    headers: {"Ehr-Session": sessionId}
			});
			$.ajax({
			    url: baseUrl + "/ehr",
			    type: 'POST',
			    success: function (data) {
			        var ehrId = data.ehrId;
			        //console.log(spol);
			        var partyData = {
			            firstNames: ime,
			            lastNames: priimek,
			            dateOfBirth: datumRojstva,
			            partyAdditionalInfo: [
			            	{key: "ehrId", value: ehrId},
			            	{key: "spol", value : spol},
			            	{key: "randSt", value : Math.floor((Math.random() * 90) + 1)}
			            ]
			        };
			        $.ajax({
			            url: baseUrl + "/demographics/party",
			            type: 'POST',
			            contentType: 'application/json',
			            data: JSON.stringify(partyData),
			            success: function (party) {
			                if (party.action == 'CREATE') {
			                    $("#kreirajSporocilo").html("<span class='obvestilo " +
	                          "label label-success fade-in'>Uspešno kreiran EHR '" +
	                          ehrId + "'.</span>");
			                    $("#preberiEHRid").val(ehrId);
			                }
			            },
			            error: function(err) {
			            	$("#kreirajSporocilo").html("<span class='obvestilo label " +
	                    "label-danger fade-in'>Napaka '" +
	                    JSON.parse(err.responseText).userMessage + "'!");
			            }
			        });
			    }
			});
		}
		if(ime.length==0){
	      	$("#labelIme").removeClass().addClass("label label-danger");
	      }
	      
	    else{
	  		$("#labelIme").removeClass().addClass("label label-success");
	     }
	     
	     if(priimek.length==0){
	      	$("#labelPriimek").removeClass().addClass("label label-danger");
	      }
	      
	    else{
	  		$("#labelPriimek").removeClass().addClass("label label-success");
	     }
	     
	     if(datumRojstva.length==0){
	      	$("#labelRojstvo").removeClass().addClass("label label-danger");
	      }
	      
	    else{
	  		$("#labelRojstvo").removeClass().addClass("label label-success");
	     }
	     
	     if(spol.length==0){
	      	$("#labelSpol").removeClass().addClass("label label-danger");
	      }
	      
	    else{
	  		$("#labelSpol").removeClass().addClass("label label-success");
	     }
	     
	     
	     
	     setTimeout(function(){ 
	     	$("#labelIme").removeClass().addClass("label label-warning");
	     	$("#labelPriimek").removeClass().addClass("label label-warning");
	     	$("#labelRojstvo").removeClass().addClass("label label-warning");
	     	$("#labelSpol").removeClass().addClass("label label-warning");
	     },500);
	}
	else if(st==1){
			sessionId = getSessionId();
			$.ajaxSetup({
			    headers: {"Ehr-Session": sessionId}
			});
			$.ajax({
			    url: baseUrl + "/ehr",
			    type: 'POST',
			    success: function (data) {
			        var ehrId = data.ehrId;
			        //console.log(spol);
			        var partyData = {
			            firstNames: musketirji[0][0],
			            lastNames: musketirji[0][1],
			            dateOfBirth: musketirji[0][2],
			            partyAdditionalInfo: [
			            	{key: "ehrId", value: ehrId},
			            	{key: "spol", value : musketirji[0][3]},
			            	{key: "randSt", value : Math.floor((Math.random() * 90) + 1)}
			            ]
			        };
			        $.ajax({
			            url: baseUrl + "/demographics/party",
			            type: 'POST',
			            contentType: 'application/json',
			            data: JSON.stringify(partyData),
			            async:true,
			            success: function (party) {
			                if (party.action == 'CREATE') {
			                	musketirji[0][4]=ehrId;
			                    var element=document.getElementById("preberiObstojeciEHR");
								element.options[1] = new Option('Janez Novak', musketirji[0][4]);
			                }
			            },
			            error: function(err) {
			            	$("#kreirajSporocilo").html("<span class='obvestilo label " +
	                    "label-danger fade-in'>Napaka '" +
	                    JSON.parse(err.responseText).userMessage + "'!");
			            }
			        });
			    }
			});
			
			//sessionId = getSessionId();
			$.ajaxSetup({
			    headers: {"Ehr-Session": sessionId}
			});
			$.ajax({
			    url: baseUrl + "/ehr",
			    type: 'POST',
			    success: function (data) {
			        var ehrId = data.ehrId;
			        musketirji[1][4]=ehrId;
			        //console.log(musketirji[1])
			        //console.log(spol);
			        var partyData = {
			            firstNames: musketirji[1][0],
			            lastNames: musketirji[1][1],
			            dateOfBirth: musketirji[1][2],
			            partyAdditionalInfo: [
			            	{key: "ehrId", value: ehrId},
			            	{key: "spol", value : musketirji[1][3]},
			            	{key: "randSt", value : Math.floor((Math.random() * 90) + 1)}
			            ]
			        };
			        $.ajax({
			            url: baseUrl + "/demographics/party",
			            type: 'POST',
			            contentType: 'application/json',
			            data: JSON.stringify(partyData),
			            success: function (party) {
			                if (party.action == 'CREATE') {
			                    musketirji[1][4]=ehrId;
			                    var element=document.getElementById("preberiObstojeciEHR");
								element.options[2] = new Option('Marija Stare', musketirji[1][4]);
			                }
			            },
			            error: function(err) {
			            	$("#kreirajSporocilo").html("<span class='obvestilo label " +
	                    "label-danger fade-in'>Napaka '" +
	                    JSON.parse(err.responseText).userMessage + "'!");
			            }
			        });
			    }

			});
			
			sessionId = getSessionId();
			$.ajaxSetup({
			    headers: {"Ehr-Session": sessionId}
			});
			$.ajax({
			    url: baseUrl + "/ehr",
			    type: 'POST',
			    success: function (data) {
			        var ehrId = data.ehrId;
			        musketirji[2][4]=ehrId;
			        //console.log(musketirji[2])
			        //console.log(spol);
			        var partyData = {
			            firstNames: musketirji[2][0],
			            lastNames: musketirji[2][1],
			            dateOfBirth: musketirji[2][2],
			            partyAdditionalInfo: [
			            	{key: "ehrId", value: ehrId},
			            	{key: "spol", value : musketirji[2][3]},
			            	{key: "randSt", value : Math.floor((Math.random() * 90) + 1)}
			            ]
			        };
			        $.ajax({
			            url: baseUrl + "/demographics/party",
			            type: 'POST',
			            contentType: 'application/json',
			            data: JSON.stringify(partyData),
			            success: function (party) {
			                if (party.action == 'CREATE') {
			                    musketirji[2][4]=ehrId;
			                    var element=document.getElementById("preberiObstojeciEHR");
								element.options[3] = new Option("Zdravko Svetovljan", musketirji[2][4]);
			                }
			            },
			            error: function(err) {
			            	$("#kreirajSporocilo").html("<span class='obvestilo label " +
	                    "label-danger fade-in'>Napaka '" +
	                    JSON.parse(err.responseText).userMessage + "'!");
			            }
			        });
			    }
			});
		
	}
}

/**
 * Za podan EHR ID preberi demografske podrobnosti pacienta in izpiši sporočilo
 * s pridobljenimi podatki (ime, priimek in datum rojstva).
 */
function preberiEHRodBolnika() {
	sessionId = getSessionId();

	var ehrId = $("#preberiEHRid").val();

	if (!ehrId || ehrId.trim().length == 0) {
		$("#preberiSporocilo").html("<span class='obvestilo label label-warning " +
      "fade-in'>Prosim vnesite zahtevan podatek!");
      setTimeout(function(){
      	$("#preberiSporocilo").html("<span class=''>");
      }, 2500);
	} else {
		$.ajax({
			
			url: baseUrl + "/demographics/ehr/" + ehrId + "/party",
			type: 'GET',
			headers: {"Ehr-Session": sessionId},
	    	success: function (data) {
				var party = data.party;
				
			//	$("#preberiSporocilo").html("<span class='obvestilo label " +
          //"label-success fade-in'>Bolnik '" + party.firstNames + " " +
          //party.lastNames + "', ki se je rodil '" + party.dateOfBirth +
          //"'.</span>");
        	
        	if($("#jok").val()==undefined){
		        	var element = document.createElement("p");
					var telo = document.getElementById("pripni");
					telo.appendChild(element);
					//party.firstNames + party.lastNames  + party.dateOfBirth
					var htmlStvar = document.createElement("a");
					htmlStvar.setAttribute("id", "jok");
					var target=party.partyAdditionalInfo;
					var spol=najdi("spol", target);
					var njegovID=najdi("ehrId",target);
						htmlStvar.innerHTML=
			      			
			      			
			      	"<font color='black' face= verdana size=2>"+
								"<div class='col-lg-6 col-md-6 col-sm-6>"+
			      					"<div class='panel panel-default'>"+"<h3>Osebna izkaznica: </h3>"+"<a class='pull-right' href='#'>\ <img class='media-object img-circle'\ src='https://randomuser.me/api/portraits/"+spol+"/"+ najdi("randSt",target)  + ".jpg'/>\</a>"+
										"<i>Ime: </i> "+ "<b>"+party.firstNames +"</b></br>"+
										"<i>Priimek: </i>"+ "<b>"+party.lastNames+"</b></br>"+
										"<i>Starost: </i>"+ "<b>"+(2017 - party.dateOfBirth.slice(0,4))+"</b></br>"+
										"<i>EHR Id: </i>"+ "<b>"+najdi("ehrId",target)+"</b></br>"+
										"<button type='button' class='btn btn-primary gradient' onclick='preberiMeritveVitalnihZnakov()'>Preberi meritve vitalnih znakov</button><span id='preberiMeritveVitalnihZnakovSporocilo'></span>"+
										"   <button type='button' class='btn btn-primary gradient' onclick='zapriPogled()'>Končaj pogled</button>"+
									"</div>"+
								"</div>"+
								"</br><br>"+
							"</font></br>";
							
							//console.log(data.get("spol"));
						
						element.appendChild(htmlStvar);
						
						
						

        	}else{
        		$("#preberiSporocilo").html("<span class='obvestilo label label-danger fade-in'>Napaka najprej končajte pogled trenutnega pacienta</span>");
          
        	}
			
			},
			error: function(err) {
				$("#preberiSporocilo").html("<span class='obvestilo label " +
          "label-danger fade-in'>Napaka '" +
          JSON.parse(err.responseText).userMessage + "'!");
			}
			
		});
		
		setTimeout(function(){ 
			 $("#preberiSporocilo").html("");
			}, 3000);
	}
}


/**
 * Pridobivanje vseh zgodovinskih podatkov meritev izbranih vitalnih znakov
 * (telesna temperatura, filtriranje telesne temperature in telesna teža).
 * Filtriranje telesne temperature je izvedena z AQL poizvedbo, ki se uporablja
 * za napredno iskanje po zdravstvenih podatkih.
 */
function preberiMeritveVitalnihZnakov() {
	sessionId = getSessionId();
	
	var ehrId = $("#preberiEHRid").val();
	var vroc=0;
	var hlad=0;
	var tezak=0;
	var suh=0;
	var pritisk=0;
	var visina=-1;
	
	$("#pripni1").append("<p><div id='pripni2'></div></p>")

	if (!ehrId || ehrId.trim().length == 0) {
		$("#preberiMeritveVitalnihZnakovSporocilo").html("<span class='obvestilo " +
      "label label-warning fade-in'>Prosim vnesite zahtevan podatek!");
	}else if(pogled==1){
		
	} 
	
	else {
		pogled=1;
		$.ajax({
			url: baseUrl + "/demographics/ehr/" + ehrId + "/party",
	    	type: 'GET',
	    	headers: {"Ehr-Session": sessionId},
	    	success: function (data) {
				var party = data.party;
				$("#rezultatMeritveVitalnihZnakov").html("<br/><span>Pridobivanje " +
          "podatkov za <b>'" + "'</b> bolnika <b>'" + party.firstNames +
          " " + party.lastNames + "'</b>.</span><br/><br/>");
          
        			
        			$.ajax({
  					    url: baseUrl + "/view/" + ehrId + "/" + "height",
					    type: 'GET',
					    headers: {"Ehr-Session": sessionId},
					    success: function (res) {
					    	var povp=0.0;
        					if(res.length>0){
        						for(var i in res){
        							povp+=res[i].height;
        						}
        						
        					}else{
        						console.log("Ni meritev");
        					}
        					visina=povp/res.length;
        				}
        			})
        			
		
					$.ajax({
  					    url: baseUrl + "/view/" + ehrId + "/" + "body_temperature",
					    type: 'GET',
					    headers: {"Ehr-Session": sessionId},
					    success: function (res) {
					    	if (res.length > 0) {
					    		var tabelaTemp=new Array(res.length);
					    		var tabelaTm=new Array(res.length);
					    		var povp=0;
					    		
					    		for(var i in res){
					    			tabelaTemp[i]=res[i].temperature
					    			tabelaTm[i]=res[i].time
					    			povp+=res[i].temperature
					    		}
					    		povp=povp/res.length;
					    		//console.log(povp);
					    		
					    		tabelaTemp.reverse();
					    		tabelaTm.reverse();
						    	var results = "</br></br></br><h4><div>Temperatura[°C]/Meritve <h4></div><canvas id='myChart' width='1200' height='500'></canvas>";
						    	$("#pripni2").append(results)
						        var ctx = document.getElementById('myChart').getContext('2d');
								var chart = new Chart(ctx, {
								    // The type of chart we want to create
								    type: 'line',
								
								    // The data for our dataset
								    data: {
								        labels: tabelaTm,
								        datasets: [{
								            label: "Temperatura",
								            backgroundColor: 'rgb(254,254 ,120 )',
								            borderColor: 'rgb(19, 19, 19)',
								            data: tabelaTemp,
								        }]
								    },
								
								    // Configuration options go here
								    options: {
								    	responsive: false
								    }
								});
								initScroll("pripni2",1200);
								if(povp<36){
									$("#pripni2").append("<div><font face=verdana color='black' size=3>Meritve kažejo na podhladitev, pacient naj se posvetuje z zdravnikom.</font></div>");
								}else if(povp>37){
									$("#pripni2").append("<div><font face=verdana color='black' size=3>Meritve nakazujejo na povišano temperaturo, pacient naj se posvetuje z zdravnikom.</font></div>");
								}else{
									$("#pripni2").append("<div><font face=verdana color='black' size=3>Meritve kažejo na sprejemljivo stanje.</font></div>");
								}
					    	} else {
					    		$("#preberiMeritveVitalnihZnakovSporocilo").html(
                    "<span class='obvestilo label label-warning fade-in'>" +
                    "Ni podatkov temperatur!</span>");
					    	}
					    },
					    error: function() {
					    	$("#preberiMeritveVitalnihZnakovSporocilo").html(
                  "<span class='obvestilo label label-danger fade-in'>Napaka '" +
                  JSON.parse(err.responseText).userMessage + "'!");
					    }
					});
					
				setTimeout(function(){	
					$.ajax({
					    url: baseUrl + "/view/" + ehrId + "/" + "weight",
					    type: 'GET',
					    headers: {"Ehr-Session": sessionId},
					    success: function (res) {
					    	if (res.length > 0) {
						    	var tabelaKG=new Array(res.length);
					    		var tabelaTm=new Array(res.length);
					    		
					    		var povpr=0;
					    		for(var i=0; i<res.length;i++){
					    			tabelaKG[i]=res[i].weight
					    			tabelaTm[i]=res[i].time
					    			povpr+=res[i].weight;
					    		}
					    		povpr=povpr/res.length;
					    		
					    		
					    		tabelaKG.reverse()
					    		tabelaTm.reverse()
						    	var results = "</br></br></br><h4><div>Teza[kg]/Meritve <h4></div><canvas id='myChart1' width='1200' height='500'></canvas>";
						    	$("#pripni2").append(results);
						        var ctx = document.getElementById('myChart1').getContext('2d');
								var chart = new Chart(ctx, {
								    // The type of chart we want to create
								    type: 'line',
								
								    // The data for our dataset
								    data: {
								        labels: tabelaTm,
								        datasets: [{
								            label: "Teza",
								            backgroundColor: 'rgb(100,254 ,255 )',
								            borderColor: 'rgb(50, 250, 50)',
								            data: tabelaKG,
								        }]
								    },
								
								    // Configuration options go here
								    options: {
								    	responsive: false
								    }
								});
								if(visina>0){
					    			visina=visina-100;
					    			visina=visina-povpr
					    			//console.log(visina)
					    			if(visina>20){
										$("#pripni2").append("<div><font face=verdana color='black' size=3>Meritve kažejo da, je pacient pretežak. Pacient naj preveri kako se prehranjuje in giba.</font></div>");
					    			}else if(visina<-15){
					    				$("#pripni2").append("<div><font face=verdana color='black' size=3>Meritve kažejo da, je pacientova teža premajhna pacient naj se posvetuje z zdravnikom.</font></div>");
					    			}else{
					    				$("#pripni2").append("<div><font face=verdana color='black' size=3>Meritve kažejo da, je pacientova teža ustrezna.</font></div>");
					    			}
					    		}
					    	} else {
					    		$("#preberiMeritveVitalnihZnakovSporocilo").html(
                    "<span class='obvestilo label label-warning fade-in'>" +
                    "Ni podatkov teže!</span>");
					    	}
					    },
					    error: function() {
					    	$("#preberiMeritveVitalnihZnakovSporocilo").html(
                  "<span class='obvestilo label label-danger fade-in'>Napaka '" +
                  JSON.parse(err.responseText).userMessage + "'!");
					    }
					});
				},1000);
				

					$.ajax({
					    url: baseUrl + "/view/" + ehrId + "/blood_pressure",
					    type: 'GET',
					    headers: {"Ehr-Session": sessionId},
					    success: function (res) {
					    	if (res.length > 0) {
						    	var tabelaKG=new Array(res.length);
					    		var tabelaTm=new Array(res.length);
					    		var avg=0;
					    		for(var i in res){
					    			tabelaKG[i]=res[i].systolic
					    			tabelaTm[i]=res[i].time
					    			avg+=res[i].systolic
					    		}
					    		avg=avg/res.length;
					    		tabelaKG.reverse();
					    		tabelaTm.reverse();
						    	var results = "</br></br></br><h4><div>Sistolični krvni pritisk/Meritve <h4></div><canvas id='myChart2' width='1200' height='500'></canvas>";
						    	$("#pripni2").append(results);
						        var ctx = document.getElementById('myChart2').getContext('2d');
								var chart = new Chart(ctx, {
								    // The type of chart we want to create
								    type: 'line',
								
								    // The data for our dataset
								    data: {
								        labels: tabelaTm,
								        datasets: [{
								            label: "Krvni tlak",
								            backgroundColor: 'rgb(255,255 ,200 )',
								            borderColor: 'rgb(20, 20, 250)',
								            data: tabelaKG,
								        }]
								    },
								
								    // Configuration options go here
								    options: {
								    	responsive: false,
										scales: {
									        yAxes: [{
									            ticks: {
									                beginAtZero: false
									            }
									        }]
									    }
								    }
								});
								
								if(avg>=130){
										$("#pripni2").append("<div><font face=verdana color='black' size=3>Meritve kažejo da, je pacientov krvni pritisk prevelik, priporočamo naj se posvetuje z zdravnikom.</font></div>");
								}else if(avg<=90){
										$("#pripni2").append("<div><font face=verdana color='black' size=3>Meritve nakazujejo na premajhen krvni pritisk, pacient naj se posvetuje z osebnim zdravnikom.</font></div>");
								}else{
										$("#pripni2").append("<div><font face=verdana color='black' size=3>Glede na meritve je pacientov krvni pritisk normalen. </font></div>");
								}
								
								
								
								
					    	} else {
					    		$("#preberiMeritveVitalnihZnakovSporocilo").html(
                    "<span class='obvestilo label label-warning fade-in'>" +
                    "Ni podatkov za krvni pritisk!</span>");
					    	}
					    },
					    error: function() {
					    	$("#preberiMeritveVitalnihZnakovSporocilo").html(
                  "<span class='obvestilo label label-danger fade-in'>Napaka '" +
                  JSON.parse(err.responseText).userMessage + "'!");
					    }
					});
					
					$.ajax({
					    url: baseUrl + "/view/" + ehrId + "/blood_pressure",
					    type: 'GET',
					    headers: {"Ehr-Session": sessionId},
					    success: function (res) {
					    	if (res.length > 0) {
						    	var tabelaKG=new Array(res.length);
					    		var tabelaTm=new Array(res.length);
					    		for(var i in res){
					    			tabelaKG[i]=res[i].diastolic
					    			tabelaTm[i]=res[i].time
					    		}
					    		tabelaKG.reverse();
					    		tabelaTm.reverse();
						    	var results = "</br></br></br><h4><div>Diastolični krvni pritisk/Meritve <h4></div><canvas id='myChart3' width='1200' height='500'></canvas>";
						    	$("#pripni2").append(results);
						        var ctx = document.getElementById('myChart3').getContext('2d');
								var chart = new Chart(ctx, {
								    // The type of chart we want to create
								    type: 'line',
								
								    // The data for our dataset
								    data: {
								        labels: tabelaTm,
								        datasets: [{
								            label: "Krvni tlak",
								            backgroundColor: 'rgb(200,200 ,200 )',
								            borderColor: 'rgb(20, 20, 50)',
								            data: tabelaKG,
								        }]
								    },
								
								    // Configuration options go here
								    options: {
								    	responsive: false
								    }
								});
					    	} else {
					    		$("#preberiMeritveVitalnihZnakovSporocilo").html(
                    "<span class='obvestilo label label-warning fade-in'>" +
                    "Ni podatkov za krvni pritisk!</span>");
					    	}
					    },
					    error: function() {
					    	$("#preberiMeritveVitalnihZnakovSporocilo").html(
                  "<span class='obvestilo label label-danger fade-in'>Napaka '" +
                  JSON.parse(err.responseText).userMessage + "'!");
					    }
					});
					
				}
	    	,
	    	error: function(err) {
	    		$("#preberiMeritveVitalnihZnakovSporocilo").html(
            "<span class='obvestilo label label-danger fade-in'>Napaka '" +
            JSON.parse(err.responseText).userMessage + "'!");
	    	}
		});
	}
		setTimeout(function(){
	        $("#preberiMeritveVitalnihZnakovSporocilo").html("");
	    },3000)
}


/**
 * Za dodajanje vitalnih znakov pacienta je pripravljena kompozicija, ki
 * vključuje množico meritev vitalnih znakov (EHR ID, datum in ura,
 * telesna višina, telesna teža, sistolični in diastolični krvni tlak,
 * nasičenost krvi s kisikom in merilec).
 */
function dodajMeritveVitalnihZnakov(st) {
	if(st==0){
		sessionId = getSessionId();
	
		var ehrId = $("#dodajVitalnoEHR").val();
		var datumInUra = $("#dodajVitalnoDatumInUra").val();
		var telesnaVisina = $("#dodajVitalnoTelesnaVisina").val();
		var telesnaTeza = $("#dodajVitalnoTelesnaTeza").val();
		var telesnaTemperatura = $("#dodajVitalnoTelesnaTemperatura").val();
		var sistolicniKrvniTlak = $("#dodajVitalnoKrvniTlakSistolicni").val();
		var diastolicniKrvniTlak = $("#dodajVitalnoKrvniTlakDiastolicni").val();
		var nasicenostKrviSKisikom = $("#dodajVitalnoNasicenostKrviSKisikom").val();
	
		if (!ehrId || ehrId.trim().length == 0) {
			$("#dodajMeritveVitalnihZnakovSporocilo").html("<span class='obvestilo " +
	      "label label-warning fade-in'>Prosim vnesite zahtevane podatke!</span>");
		} else {
			$.ajaxSetup({
			    headers: {"Ehr-Session": sessionId}
			});
			var podatki = {
				// Struktura predloge je na voljo na naslednjem spletnem naslovu:
	      // https://rest.ehrscape.com/rest/v1/template/Vital%20Signs/example
			    "ctx/language": "en",
			    "ctx/territory": "SI",
			    "ctx/time": datumInUra,
			    "vital_signs/height_length/any_event/body_height_length": telesnaVisina,
			    "vital_signs/body_weight/any_event/body_weight": telesnaTeza,
			   	"vital_signs/body_temperature/any_event/temperature|magnitude": telesnaTemperatura,
			    "vital_signs/body_temperature/any_event/temperature|unit": "°C",
			    "vital_signs/blood_pressure/any_event/systolic": sistolicniKrvniTlak,
			    "vital_signs/blood_pressure/any_event/diastolic": diastolicniKrvniTlak,
			    "vital_signs/indirect_oximetry:0/spo2|numerator": nasicenostKrviSKisikom
			};
			var parametriZahteve = {
			    ehrId: ehrId,
			    templateId: 'Vital Signs',
			    format: 'FLAT'
	
			};
			$.ajax({
			    url: baseUrl + "/composition?" + $.param(parametriZahteve),
			    type: 'POST',
			    contentType: 'application/json',
			    data: JSON.stringify(podatki),
			    success: function (res) {
			        $("#dodajMeritveVitalnihZnakovSporocilo").html(
	              "<span class='obvestilo label label-success fade-in'>" +
	              res.meta.href + ".</span>");
			    },
			    error: function(err) {
			    	$("#dodajMeritveVitalnihZnakovSporocilo").html(
	            "<span class='obvestilo label label-danger fade-in'>Napaka '" +
	            JSON.parse(err.responseText).userMessage + "'!");
			    }
			});
		}
	}else{
		setTimeout(function(){
      	
  
		
		for(var sprem=0;sprem<9;sprem++){
			var ehrId;
			if(sprem<3){ehrId=musketirji[0][4];}
			else if(sprem>=3 && sprem<=5){ehrId=musketirji[1][4];}
			else{ehrId=musketirji[2][4]}
			//console.log(sprem)
			//console.log(ehrId);
			sessionId = getSessionId();
			$.ajaxSetup({
			    headers: {"Ehr-Session": sessionId}
			});
			var podatki = {
				// Struktura predloge je na voljo na naslednjem spletnem naslovu:
	      // https://rest.ehrscape.com/rest/v1/template/Vital%20Signs/example
			    "ctx/language": "en",
			    "ctx/territory": "SI",
			    "ctx/time": rezultati[sprem][0],
			    "vital_signs/height_length/any_event/body_height_length": rezultati[sprem][1],
			    "vital_signs/body_weight/any_event/body_weight": rezultati[sprem][2],
			   	"vital_signs/body_temperature/any_event/temperature|magnitude": rezultati[sprem][3],
			    "vital_signs/body_temperature/any_event/temperature|unit": "°C",
			    "vital_signs/blood_pressure/any_event/systolic": rezultati[sprem][4],
			    "vital_signs/blood_pressure/any_event/diastolic": rezultati[sprem][5],
			    "vital_signs/indirect_oximetry:0/spo2|numerator": rezultati[sprem][6]
			};
			var parametriZahteve = {
			    ehrId: ehrId,
			    templateId: 'Vital Signs',
			    format: 'FLAT'
	
			};
			$.ajax({
			    url: baseUrl + "/composition?" + $.param(parametriZahteve),
			    type: 'POST',
			    contentType: 'application/json',
			    data: JSON.stringify(podatki),
			    success: function (res) {
			        
			    },
			    error: function(err) {
			    	$("#dodajMeritveVitalnihZnakovSporocilo").html(
	            "<span class='obvestilo label label-danger fade-in'>Napaka '" +
	            JSON.parse(err.responseText).userMessage + "'!");
			    }
			});
		}
	}, 1500);
	}
}


$(document).ready(function() {

  /**
   * Napolni testne vrednosti (ime, priimek in datum rojstva) pri kreiranju
   * EHR zapisa za novega bolnika, ko uporabnik izbere vrednost iz
   * padajočega menuja (npr. Pujsa Pepa).
   */
  $('#preberiPredlogoBolnika').change(function() {
    $("#kreirajSporocilo").html("");
    var podatki = $(this).val().split(",");
    $("#kreirajIme").val(podatki[0]);
    $("#kreirajPriimek").val(podatki[1]);
    $("#kreirajDatumRojstva").val(podatki[2]);
    $("#kreirajSpol").val(podatki[3]);
  });

  /**
   * Napolni testni EHR ID pri prebiranju EHR zapisa obstoječega bolnika,
   * ko uporabnik izbere vrednost iz padajočega menuja
   * (npr. Dejan Lavbič, Pujsa Pepa, Ata Smrk)
   */
	$('#preberiObstojeciEHR').change(function() {
		$("#preberiSporocilo").html("");
		$("#preberiEHRid").val($(this).val());
	});

  /**
   * Napolni testne vrednosti (EHR ID, datum in ura, telesna višina,
   * telesna teža, telesna temperatura, sistolični in diastolični krvni tlak,
   * nasičenost krvi s kisikom in merilec) pri vnosu meritve vitalnih znakov
   * bolnika, ko uporabnik izbere vrednosti iz padajočega menuja (npr. Ata Smrk)
   */
	$('#preberiObstojeciVitalniZnak').change(function() {
		$("#dodajMeritveVitalnihZnakovSporocilo").html("");
		var podatki = $(this).val().split("|");
		$("#dodajVitalnoEHR").val(podatki[0]);
		$("#dodajVitalnoDatumInUra").val(podatki[1]);
		$("#dodajVitalnoTelesnaVisina").val(podatki[2]);
		$("#dodajVitalnoTelesnaTeza").val(podatki[3]);
		$("#dodajVitalnoTelesnaTemperatura").val(podatki[4]);
		$("#dodajVitalnoKrvniTlakSistolicni").val(podatki[5]);
		$("#dodajVitalnoKrvniTlakDiastolicni").val(podatki[6]);
		$("#dodajVitalnoNasicenostKrviSKisikom").val(podatki[7]);
	});

  /**
   * Napolni testni EHR ID pri pregledu meritev vitalnih znakov obstoječega
   * bolnika, ko uporabnik izbere vrednost iz padajočega menuja
   * (npr. Ata Smrk, Pujsa Pepa)
   */
	$('#preberiEhrIdZaVitalneZnake').change(function() {
		$("#preberiMeritveVitalnihZnakovSporocilo").html("");
		$("#rezultatMeritveVitalnihZnakov").html("");
		$("#preberiEHRid");
	});
});



/**
 * Generator podatkov za novega pacienta, ki bo uporabljal aplikacijo. Pri
 * generiranju podatkov je potrebno najprej kreirati novega pacienta z
 * določenimi osebnimi podatki (ime, priimek in datum rojstva) ter za njega
 * shraniti nekaj podatkov o vitalnih znakih.
 * @param stPacienta zaporedna številka pacienta (1, 2 ali 3)
 * @return ehrId generiranega pacienta
 */
function generirajPodatke() {
	kreirajEHRzaBolnika(1);
	
	var element = document.getElementById('preberiPredlogoBolnika');
	
	element.options[1] = new Option('Janez Novak', 'Janez,Novak,1960-10-11,Moški');
	element.options[2] = new Option('Marija Stare', "Marija,Stare,1956-01-01 ,Ženski");
	element.options[3] = new Option("Zdravko Svetovljan","Zdravko,Svetovljan,1971-05-23,Moški");
	dodajMeritveVitalnihZnakov(3);
	
	
	element=document.getElementById('preberiObstojeciVitalniZnak')
	
	
	
	//Generiraj rand data
	
	
}

/**
 * najdi pravi podatek
 */
 function najdi(key, target){
 	for(var k in target){
 		if(target[k].key.localeCompare(key)==0){
 			return target[k].value;
 		}
 	}
 	return -1;
 }
 
 function zapriPogled(htmlStvar) {
 		$("#jok").remove();
 		$("#pripni2").remove();
 		pogled=0;
 }

function initScroll(elementid, margY){
	var destination= document.getElementById(elementid).offsetTop;
	var scroller=setTimeout(function(){
		initScroll(elementid, margY);
	},1);
	var margY = margY + 8;
	if(margY >=destination){
		clearTimeout(scroller);
	}
	window.scroll(0,margY);
}

// TODO: Tukaj implementirate funkcionalnost, ki jo podpira vaša aplikacija

var map;
var infowindow;

function initialize() {
	var lat=46.05108;
	var lng=14.50513;
	
	if (navigator.geolocation) {
    	navigator.geolocation.getCurrentPosition(function (p) {
       

	var pyrmont =new google.maps.LatLng(p.coords.latitude, p.coords.longitude); 
	
	  map = new google.maps.Map(document.getElementById('map-canvas'), {
	    center: pyrmont,
	    zoom: 16
	  });
	
	  var request = {
	    location: pyrmont,
	    radius: 5000,
	    types: ['hospital', 'health', 'physiotherapist', 'pharmacy'] 
	  };
	  infowindow = new google.maps.InfoWindow();
	  var service = new google.maps.places.PlacesService(map);
	  service.nearbySearch(request, callback);
		});
	}else{
		console.log("Ni geo točke")
	}
}

function callback(results, status) {
  if (status == google.maps.places.PlacesServiceStatus.OK) {
    for (var i = 0; i < results.length; i++) {
      createMarker(results[i]);
    }
  }
}

function createMarker(place) {
  var placeLoc = place.geometry.location;
  var marker = new google.maps.Marker({
    map: map,
    position: place.geometry.location
  });

  google.maps.event.addListener(marker, 'click', function() {
    infowindow.setContent(place.name);
    infowindow.open(map, this);
  });
}
google.maps.event.addDomListener(window, 'load', initialize);